
def get_minmax_bucket(s):
    return s.value_counts().idxmax()


def minmax_buckets(start=0, stop=10):
    # Takes a range (start and stop point) - Constructs function which min-maxes then returns that range (eg 20-30th min max)

    def min_max(df):
        ecs_df = df.apply(lambda x: x.value_counts())  # This is a slowish function (20s?) and could be precaled
        ecs_df = ecs_df.fillna(0)
        top_n = ecs_df.max().sort_values()[start:stop].index
        return top_n
    return min_max


def brute_force(df):
    return df.index


def n_random(n):
    return lambda df: df.sample(n, replace=True).index

def presets(wordlist=[]):  # Constructs a function which returns the preset wordlist everytime
    return lambda x: wordlist
