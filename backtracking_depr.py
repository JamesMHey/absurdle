import logging

from preproc_depr import load_equivalence_class_matrix
from simulator import AbsurdleSimulator

MAX_DEPTH = 4

def solve(a):
    depth = len(a.history.keys())
    if depth >= MAX_DEPTH:
        return
    solutions_df = a.solutions_df
    if len(solutions_df) == 1:  # Recursive base case
        solutions = list(a.history.keys())
        solutions.append(a.solutions_df.index[0])
        print(f'SOLUTION = {solutions}')
        logging.info(solutions)
        a.revert()
        return

    def backtrack(word):
        a.guess(word)
        solve(a)
        a.revert()

    solutions_df = a.solutions_df

    if depth < 2:  # Initial search based on
        ecs_df = solutions_df.apply(lambda x: x.value_counts())  # This is the slowest function and could be precaled
        ecs_df = ecs_df.fillna(0)
        top_n = ecs_df.max().sort_values()[0:20].index
        for word in top_n:
            backtrack(word)
    else:
        for word in solutions_df.index:
            backtrack(word)

if __name__ == '__main__':
    logging.basicConfig(filename='solutions.log', level=logging.INFO)
    df = load_equivalence_class_matrix()
    a = AbsurdleSimulator(df)
    solve(a)