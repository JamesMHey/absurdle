
from backtracking import Backtracker
from selection_functions import presets, brute_force, n_random, minmax_buckets

b = Backtracker()
b.run()

SOLUTION = ['AIERY', 'COAST', 'MANGA', 'PUPAL']
SOLUTION = ['AYRIE', 'COAST', 'MANGA', 'PUPAL']

#  Take 2 known good leaf words and check 100 top root words
settings = {0: minmax_buckets(0, 100),
            1: presets(['COAST']),
            2: presets(['MANGA'])}
b = Backtracker(settings)
b.run()

settings = {0: minmax_buckets(0, 1000),
            1: presets(['COAST']),
            2: presets(['MANGA'])}
b = Backtracker(settings)
b.run()