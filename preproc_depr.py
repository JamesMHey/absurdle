from pathlib import Path
import numpy as np
import pandas as pd

from helpers import get_valid_words
from helpers import timeit


def save_equivalence_class(word_list=None, save_path=Path('EC_matrix.csv')):

    if word_list is None:
        word_list = get_valid_words('np')

    matrix = equiv_class_matrix(word_list)
    matrix.to_csv(save_path)
    return matrix

@timeit
def load_equivalence_class_matrix(save_path=Path('EC_matrix_b3.pkl')):
    return pd.read_pickle(save_path)


def equiv_class_matrix(word_list):
    """
    Takes a word list and returns a matrix of equivilance scores across the list.
    :param word_list: n x word length matrix of letters
    :return:
    """
    words_as_strings = flatten(word_list)
    equivs_df = pd.DataFrame(index=words_as_strings)
    for word in equivs_df.index:
        vals = equivs_df.reset_index()['index'].apply(lambda x: equiv_guess_single(word, x))
        vals.index = equivs_df.index
        vals.name = word
        vals = vals.astype('category')
        equivs_df = pd.concat([equivs_df, vals], axis=1)
        #equivs_df[word] = vals
    return equivs_df


def equiv_guess_single(guess: str, comparison_word: np.array):
    """
    :param guess: The guess word
    :param comparison_word: A single comparison word
    :return: list of 'G', 'Y' and 'B' values for the pairing
    """
    return ''.join([score_col(letter, comparison_word, index) for index, letter in enumerate(guess)])


def score_col(letter: str, comparison_word, index: int):
    """
    :param letter: A given letter in position index
    :param comparison_word: The word to compare against
    :param index: The position of the index
    :return: 'G', 'Y' or 'B' as string
    """
    if letter == comparison_word[index]:
        return 'G'
    if letter in comparison_word:
        return 'Y'
    return 'B'


def flatten(arr):  # Flattens 2d array into strings
    """
    :param arr: 2d np array of words (row) and letters (col)
    :return: 1d np array of strings
    """
    return np.apply_along_axis(lambda x: ''.join(x), axis=1, arr=arr)


def get_unique_ecs(responses='GBY', word_length=5):
    # Calculates unique ecs combinations
    for letter in range(word_length):
        unique_ecs = [i for i in responses]
        for y in range(letter):
            unique_ecs = [x+i for i in responses for x in unique_ecs]

    return unique_ecs