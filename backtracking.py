import logging
from functools import partial
from multiprocessing import Process

from helpers import load_equivalence_class_matrix, timeit
from selection_functions import minmax_buckets, brute_force
from simulator import AbsurdleSimulator
from globals import DEFAULT_SOLUTIONS_FILE



class Backtracker():
    MAX_DEPTH = 4
    DEFAULT_ROOT_RANGE = (0, 10)

    DEFAULT_DEPTH_SPECIFICATIONS = {  # Selection function to be used at differen levels
        0: minmax_buckets(DEFAULT_ROOT_RANGE[0], DEFAULT_ROOT_RANGE[1]),  # First 10 root words
        1: brute_force,
        2: brute_force,
        3: brute_force

    }

    def __init__(self, depth_specifications={}):
        df = load_equivalence_class_matrix()
        self.checked = 0
        self.a = AbsurdleSimulator(df)
        self.DEPTH_SPECIFICATIONS = dict(self.DEFAULT_DEPTH_SPECIFICATIONS)
        self.DEPTH_SPECIFICATIONS.update(depth_specifications)  # Add custom selection functions
        if len(logging.getLoggerClass().root.handlers) == 0:  # If logger is not set
            logging.basicConfig(filename=DEFAULT_SOLUTIONS_FILE, level=logging.INFO)

    def solve(self, a):

        depth = len(self.a.history.keys())
        if depth >= self.MAX_DEPTH:  # If the solution is > MAX_DEPTH (default 4) then discard it
            self.checked += 1
            return

        solutions_df = a.solutions_df
        if len(solutions_df) == 1:  # Recursive base case (solution)
            solutions = list(a.history.keys())
            solutions.append(a.solutions_df.index[0])
            print(f'SOLUTION = {solutions}')  # Print solution
            logging.info(solutions)  # Log solution to file (thread safe)
            self.checked += 1
            return

        solutions_df = a.solutions_df
        wordlist_function = self.DEPTH_SPECIFICATIONS.get(depth)  # selection function
        for word in wordlist_function(solutions_df):
            self.a.guess(word)

            self.solve(a)
            self.a.revert()

    @timeit
    def run(self):
        self.solve(self.a)
        print(f"Words checked: {self.checked}")


if __name__ == '__main__':
    b = Backtracker()
    b.run()