from multiprocessing import Process
import logging
from backtracking import Backtracker
from selection_functions import minmax_buckets

def run(range):
    logging.basicConfig(filename=f'solutions_{range}.log', level=logging.INFO)
    settings = {0: minmax_buckets(range[0], range[1])}
    b = Backtracker(settings)
    b.run()

if __name__ == '__main__':
    ranges = zip(range(0, 1000, 100), range(100, 1100, 100)) # Search top 100 words (by minmax bucket) in intervals of 10
    pl = [Process(target=run, args=(valrange,)) for valrange in ranges]
    for p in pl:
        p.start()

    for p in pl: # Prevent zombie processes
        p.join()
