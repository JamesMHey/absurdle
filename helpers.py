import csv
import time

import numpy as np
import pandas as pd


def load_equivalence_class_matrix(save_path=None):
    if save_path is None:
        from globals import EC_MATRIX_PATH
        save_path = EC_MATRIX_PATH
    return pd.read_pickle(save_path)


def load_root_minmax_buckets(save_path=None):
    if save_path is None:
        from globals import MINMAX_BUCKETS_PATH
        save_path = MINMAX_BUCKETS_PATH
    return pd.read_pickle(save_path)


def preproc_root_minmax_buckets(save_path=None):  # Preprocesses and saves the max bucket sizes of root guesses
    if save_path is None:
        from globals import MINMAX_BUCKETS_PATH
        save_path = MINMAX_BUCKETS_PATH
    df = load_equivalence_class_matrix()
    df = df.loc[:, get_solution_words('pd')].transpose()  # Filter for only solution words
    df = df.apply(lambda x: x.value_counts())  # Calculate bin sizes
    df = df.fillna(0)  #
    df = df.max().sort_values()  # Maximum bin sizes, reverse ordered
    df.to_pickle(save_path)  # Save

def get_valid_words(return_type='pd'):
    from globals import VALID_WORDS_PATH
    if return_type == 'np':  # Return a NP array or words x letters
        return csv_to_np(VALID_WORDS_PATH)
    elif return_type == 'pd':  # Return a pandas series
        return pd.read_csv(VALID_WORDS_PATH, names=['solution_words']).squeeze()


def get_solution_words(return_type='pd'):
    from globals import SOLUTION_WORDS_PATH
    if return_type == 'np':  # Return a NP array or words x letters
        return csv_to_np(SOLUTION_WORDS_PATH)
    elif return_type == 'pd':  # Return a pandas series
        return pd.read_csv(SOLUTION_WORDS_PATH, names=['solution_words']).squeeze()


def csv_to_np(path):  # Takes path of wordlist CSV and returns NP array of letters
    from globals import WORD_LENGTH
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        words = []
        for row in reader:
            word = row[0]
            if len(word) == WORD_LENGTH:
                words.append(list(word))
            else:
                raise ImportError(f"Incorrect Word Length. Length {WORD_LENGTH} expected")

    return np.array(words)


def timeit(f):  # Decorator to time function/method calls. Adapted from a stack overflow post
    def timed(*args, **kw):

        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        print('{} took: {} sec'.format(f.__name__, te-ts))
        return result

    return timed


