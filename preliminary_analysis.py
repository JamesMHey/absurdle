# Just some initial analysis to investigate the word list
from pathlib import Path

import pandas as pd

from helpers import get_solution_words, load_equivalence_class_matrix


df = load_equivalence_class_matrix()  # Note - 300mb that takes a lot of time to load

solution_words = get_solution_words(return_type='pd')
sols_df = df.loc[:, solution_words].transpose()

# Question 2

uecs = sols_df.nunique()  # Number of unique equivalence classes of the solution words for each possible guess
uecs.sort_values(ascending=False)[0:10]  # Unique eq classes (answer to Q2)


# Question 3:

#ecs_df = pd.DataFrame(index=get_unique_ecs())
ecs_df = sols_df.apply(lambda x: x.value_counts())  # Count number of eq classes of each guess
ecs_df = ecs_df.fillna(0)  # Fix NAs
top_10 = ecs_df.max().sort_values()[0:10]  # Top 10 minimum max eqs
top_10_ecs = ecs_df[ecs_df.max().sort_values()[0:10].index].apply(lambda x: x.idxmax())
pd.DataFrame({'Bin': top_10_ecs, 'Max Bin Size': top_10.astype(int)})  # For results
minmax = ecs_df.max().idxmin()  # Word with the minimum maximum eq class
minmax_eq_class = sols_df[minmax].value_counts().idxmax()  # Equivilance class of word
sols_df[sols_df[minmax] == minmax_eq_class]  # Remaining guess/solution matrix after guesses
sols_df = sols_df[sols_df[minmax] == minmax_eq_class]
ecs_df = ecs_df.transpose()