
# Absurdle Solver

A simple tool to find and test [absurdle](https://qntm.org/files/absurdle/absurdle.html) solutions using a backtracking algorithm. The interface allows for custom pruning functions such as minmax buckets, random selections or guessing specific words in given positions. 


## Environment set up

Create a virtual environment (optional) `python3 -m venv venv `

Activate environment `source venv/bin/activate`

Install requirements `pip install -r requirements.txt `

## Preprocessing Equivilence Classes 

The equivilance class matrix has been precalculated and is saved in EC_matrix_b3.pkl

An original attempt at generating these codes can be found in precroc_depr.py, but due to the strange edgecase handling (such as with repreated letters) I decided to adapt 3b1b's implementation from: https://github.com/3b1b/videos/tree/master/_2022/wordle to calculate the equivalence classes 

The primary code for generating them is found in preprocess.py and can be run:

```python
from preprocess import generate_full_pattern_matrix
generate_full_pattern_matrix()
```

To import them:
```python
from helpers import load_equivalence_class_matrix
df = load_equivalence_class_matrix()
```

The rows of this matrix represent potential guesses. The columns are potential solutions and the values represent the response wordle would give if that guess was made for a given solution. 

```python
df.loc[:, 'FALLS']  # Response from all possible guesses if the solution was 'FALLS'
df.loc[:, 'FALLS'].sample(5)  # View the responses of 5 random guesses
```


| Guess  |Response|
|:------:|:---:| 
| TAXER  |BGBBB|
| PIROG  |BBBBB|
| TILLS  |BBGGG|
| GLOBI  |BYBBB|
| COMBI  |BBBBB|




## Working with MinMax bins

Absurdle is an adversarial version of the game wordle. When each guess is input, the game selects a response which minimally divides the feasible solutions list.
Using the precalculated responses dicussed above, we can cacluate the size of each equivalence class bin as follows


```python
from helpers import load_equivalence_class_matrix
from helpers import get_solution_words 
df = load_equivalence_class_matrix() 
solutions_df = df.loc[:, get_solution_words()]  # Only valid solutions are kept
solutions_df = solutions_df.transpose()  #  Solutions move to rows and guesses to columns
bins_df = solutions_df.apply(lambda x: x.value_counts())  # Count The number of members of each EC bin for each valid guess (columns)
bins_df = bins_df.fillna(0)  # Missing equivalence classes have 0 values
```

So we've calculated the size of each equivalence classes bin. It's time to start thinking like Abserdle does! When a guess is made, abserdle seeks to keep as many solutions viable as possible, so it's going to select the largest bin for a given word. Lets try with 'FALLS' and predict what Abserdle will respond with:

```python
bins_df['FALLS'].sort_values(ascending=False).head()   # The largest EC bins for the word falls
```

| Response | Bin Size  |
|:---------|:---------:|
| BBBBB    |    649    |
| BBBBY    |    282    |
| BYBBB    |    249    |
| BGBBB    |    170    |
| BBYBB    |    139    |

We may expect that BBBBB is always the response given by Abserdle, but in fact there are instances where other bins are larger. The word 'AYRIE' for example returns:

|Response|Bin Size  |
|:-------|:--------:|
| YBBBB  |   171    |
| BBBBB  |   162    |


Since we're thinking link Abserdle, we're going to start working with only the maximum bin size for each word:

```python
import pandas as pd
max_bins_df = bins_df.max()
max_bins_df = max_bins_df.sort_values(ascending=True)  # Smallest maximum bin sizes first
max_bins_df_responses = bins_df[max_bins_df.index].apply(lambda x: x.idxmax())
pd.DataFrame({'Bin': max_bins_df_responses, 'Max Bin Size': max_bins_df.astype(int)})
```
By taking the maximum bin size for each possible guess then sorting our dataframe, we can find the 10 words with the smallest maximum bins. When we talk about selecting based on minmax, it refers to selecting words based on their ranking in this list. The first 10 words are show below: 


| Word  | Bin   |  Max Bin Size   |
|:------|:------|:---------------:|
| AESIR | BBBBB |       168       |
| ARISE | BBBBB |       168       |
| RAISE | BBBBB |       168       |
| SERAI | BBBBB |       168       |
| REAIS | BBBBB |       168       |
| AIERY | YBBBB |       171       |
| AYRIE | YBBBB |       171       |
| ARIEL | BBBBB |       173       |
| RAILE | BBBBB |       173       |
| ALOES | BBBYB |       174       |

Using `max_bins_df_responses.value_counts()` we can see how many words elicit given responses from Abserdle

| Largest Bin | Number of Words  |
|:-----------:|:----------------:|
|    BBBBB    |      12445       |
|    YBBBB    |       156        |
|    BBYBB    |       123        |
|    BBBBY    |        96        |
|    BBBYB    |        76        |
|    BYBBB    |        76        |

## Playing Absurdle

Playing a game of Absurdle is simple. The AbsurdleSimulator object has a guess method which takes a 5 character upper case word as a string and returns a response as a string. NOTE: The absurdle simulator is built primarily for programatic use and so it has minimal input validation. You can expect exceptions if you input words not in the valid word list. 

```python
from simulator import AbsurdleSimulator
a = AbsurdleSimulator
a.guess("FALSE")
>>> 'BBBBB'
```

Guesses can be undone withe the `.revert()` method and reset using `.reset()`

Multiple guesses can be provided at once, which will prompt the game to return the size of the remaining solutions list:
```python
a.guesses(['PIZZA','TESTS','WORDS', 'HONKS'])  # 4 random guesses
>>> 8
a.reset()
a.guesses(['RILEY', 'COAST', 'MANGA', 'HUMAN']) # A known solution
>>> 1 
```




## Running a Search

The simplest way to run a search is using the preconfigured solver in backtracking.py

```
from backtracking import Backtracker()
solve = Backtracker()
solver.run()
```

Using the default values will brute force the 10 most promising root words (based on the minmax selection).

The console will print solutions as they are found as well as store them in solutions.log.
The default setting selects the 10 most pomising words (based on the min-max selection process) and brute forces the rest of the search space.

This finds 2 solutions:
```python
['AIERY', 'COAST', 'MANGA', 'PUPAL']
['AYRIE', 'COAST', 'MANGA', 'PUPAL']
Words checked: 370204
run took: 269.2270402908325 sec
```
The last word is forced, but notice a common second and third word. We could try finding other root words that force the next solution. This can be achieved simply using custom selection functions.

## Custom Selection Functions 

Several functions are availible in selection_function.py

* brute_force - All remaining possible words
* minmax_buckets - Minimises the maximum bucket size 
* presets - A preset list of words to iterate through
* n_random - Random subset of possible words

Selection functions can be passed to the Backtracker as a dictionary. The Key represents the depth to use that selection function. Let us construct a search which takes the two promising leaf words and checks them against the 100 most promising initial words. 

```python
settings = {0: minmax_buckets(0, 100),
            1: presets(['COAST']),
            2: presets(['MANGA'])}
b = Backtracker(settings)
b.run()
```

This finds one additional solution, as well as redisovering the inital 2 in significantly less time.
Interestingly, the final word in the solution is different. Changing leaf words also accounts for the mismatch of words checked - Every leaf word checked is counted as a word even if it is not a solution. We can see an average of 17.16 leaf words existed per root word. 
```python
SOLUTION = ['AIERY', 'COAST', 'MANGA', 'PUPAL']
SOLUTION = ['AYRIE', 'COAST', 'MANGA', 'PUPAL']
SOLUTION = ['RILEY', 'COAST', 'MANGA', 'HUMAN']
Words checked: 1716
run took: 9.8435218334198 sec
```

Running again against the 1000 top words yielded no additional solutions: 

```python
SOLUTION = ['AIERY', 'COAST', 'MANGA', 'PUPAL']
SOLUTION = ['AYRIE', 'COAST', 'MANGA', 'PUPAL']
SOLUTION = ['RILEY', 'COAST', 'MANGA', 'HUMAN']
Words checked: 23732
run took: 46.57022833824158 sec
```

## Parallel Searches with Multiprocessing
The code used in this section can be found in backtracking_mp.py

The simplest way to achieve efficient multiprocessing is to divide the root words and process them seperatly.
The minmax_buckets selection function can be constructed with a range to return root words.

First import the nessisary components for this task 

```python
from multiprocessing import Process
import logging
from backtracking import Backtracker
from selection_functions import minmax_buckets
```

The python multiprocessing library takes a run function that forked processes use when started. We can define our run function to take a range such as (0,100) of the initial ordered minmax list as follows:
```python
def run(range):
    logging.basicConfig(filename=f'solutions_{range}.log', level=logging.INFO)
    settings = {0: minmax_buckets(range)}
    b = Backtracker(settings)
    b.run()
```
We have defined a seperate logfile for each process to conveniently observe the number of solutions in each section of the minmax list. However logging is threadsafe and using a shared log file (such as the default) will also work. 

In order to start the processes, we first divide up the search space into ranges, then construct and start the processes.
```python
ranges = zip(range(0, 1000, 100), range(100, 1100, 100))`
pl = [Process(target=run, args=(valrange,)) for valrange in ranges]
for p in pl:
    p.start()
for p in pl: # Prevent zombie processes
    p.join()
```
This starts up 10 processes, each assigned 100 root words from the first 1000 most promising minmax words, which they will brute force search.

A total of 209 solutions are found in this process, which can be seen in data/solutions_1000.txt
When run again on the next 1000 minmax words, only 55 solutions are found. 

## Running tests

A small suite of basic unittests have been included in test/test_absurdal.py
These can be run from the command line using
`./runtest.sh`



