import itertools as it
from pathlib import Path

import numpy as np
import pandas as pd
from globals import EC_MATRIX_PATH, PATTERN_MATRIX_FILE
from helpers import get_valid_words

# NOTE: This pattern generation code is adapted from 3b1b library to catch all the weird matching rules
# https://github.com/3b1b/videos/tree/master/_2022/wordle

PATTERN_GRID_DATA = dict()
PATTERN_MATRIX_FILE_PD = Path('EC_matrix_b3.pkl')
MISS = np.uint8(0)
MISPLACED = np.uint8(1)
EXACT = np.uint8(2)

def words_to_int_arrays(words):
    return np.array([[ord(c)for c in w] for w in words], dtype=np.uint8)

def generate_pattern_matrix(words1, words2):  # NOTE - Stolen from 3b1b
    """
    A pattern for two words represents the wordle-similarity
    pattern (grey -> 0, yellow -> 1, green -> 2) but as an integer
    between 0 and 3^5. Reading this integer in ternary gives the
    associated pattern.
    This function computes the pairwise patterns between two lists
    of words, returning the result as a grid of hash values. Since
    this can be time-consuming, many operations that can be are vectorized
    (perhaps at the expense of easier readibility), and the the result
    is saved to file so that this only needs to be evaluated once, and
    all remaining pattern matching is a lookup.
    """

    # Number of letters/words
    nl = len(words1[0])
    nw1 = len(words1)  # Number of words
    nw2 = len(words2)  # Number of words

    # Convert word lists to integer arrays
    word_arr1, word_arr2 = map(words_to_int_arrays, (words1, words2))

    # equality_grid keeps track of all equalities between all pairs
    # of letters in words. Specifically, equality_grid[a, b, i, j]
    # is true when words[i][a] == words[b][j]
    equality_grid = np.zeros((nw1, nw2, nl, nl), dtype=bool)
    for i, j in it.product(range(nl), range(nl)):
        equality_grid[:, :, i, j] = np.equal.outer(word_arr1[:, i], word_arr2[:, j])

    # full_pattern_matrix[a, b] should represent the 5-color pattern
    # for guess a and answer b, with 0 -> grey, 1 -> yellow, 2 -> green
    full_pattern_matrix = np.zeros((nw1, nw2, nl), dtype=np.uint8)

    # Green pass
    for i in range(nl):
        matches = equality_grid[:, :, i, i].flatten()  # matches[a, b] is true when words[a][i] = words[b][i]
        full_pattern_matrix[:, :, i].flat[matches] = EXACT

        for k in range(nl):
            # If it's a match, mark all elements associated with
            # that letter, both from the guess and answer, as covered.
            # That way, it won't trigger the yellow pass.
            equality_grid[:, :, k, i].flat[matches] = False
            equality_grid[:, :, i, k].flat[matches] = False

    # Yellow pass
    for i, j in it.product(range(nl), range(nl)):
        matches = equality_grid[:, :, i, j].flatten()
        full_pattern_matrix[:, :, i].flat[matches] = MISPLACED
        for k in range(nl):
            # Similar to above, we want to mark this letter
            # as taken care of, both for answer and guess
            equality_grid[:, :, k, j].flat[matches] = False
            equality_grid[:, :, i, k].flat[matches] = False

    # Rather than representing a color pattern as a lists of integers,
    # store it as a single integer, whose ternary representations corresponds
    # to that list of integers.
    pattern_matrix = np.dot(
        full_pattern_matrix,
        (3**np.arange(nl)).astype(np.uint8)
    )

    return pattern_matrix


def pattern_to_int_list(pattern):
    result = []
    curr = pattern
    for x in range(5):
        result.append(curr % 3)
        curr = curr // 3
    return result


def pattern_to_string(pattern):
    d = {MISS: "B", MISPLACED: "Y", EXACT: "G"}
    return "".join(d[x] for x in pattern_to_int_list(pattern))


def patterns_to_string(patterns):
    return "\n".join(map(pattern_to_string, patterns))

def get_word_buckets(guess, possible_words, pm):
    buckets = [[] for x in range(3**5)]
    hashes = pm().flatten()
    for index, word in zip(hashes, possible_words):
        buckets[index].append(word)
    return buckets


def generate_full_pattern_matrix():
    words = get_valid_words()
    pattern_matrix = generate_pattern_matrix(words, words)
    np.save(PATTERN_MATRIX_FILE, pattern_matrix)  # Save to file
    #pattern_matrix = np.load(str(PATTERN_MATRIX_FILE)+'.npy')
    unp = np.unique(pattern_matrix)  # Unique patterns
    replacement_dict = {p: pattern_to_string(p) for p in unp}
    df = pd.DataFrame(index=words, columns=words, data=pattern_matrix, dtype="category")
    df_str = df.replace(replacement_dict)  # This is really slow - Consider switching to using the ternery ops downstream
    df_str.to_pickle(EC_MATRIX_PATH)
    return pattern_matrix

if __name__ == '__main__':
    generate_full_pattern_matrix()