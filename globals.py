from pathlib import Path

WORD_LENGTH = 5
DEFAULT_SOLUTIONS_FILE = Path('solutions.log')
VALID_WORDS_PATH = Path('data/valid.csv')
SOLUTION_WORDS_PATH = Path('data/solutions.csv')
EC_MATRIX_PATH = Path('data/EC_matrix_b3.pkl')
PATTERN_MATRIX_FILE = Path('data/EC_matrix_np')
MINMAX_BUCKETS_PATH = Path('data/root_minmax.pkl')  # Preprocessed max bucket size for root words
VALID_WORDLIST_LENGTH = 12972  # Used for testing
SOLUTIONS_WORDLIST_LENGTH = 2315