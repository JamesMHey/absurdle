from collections import OrderedDict
from helpers import get_solution_words, load_equivalence_class_matrix

#  Simulator for Absurdal game


class AbsurdleSimulator:

    def __init__(self, word_df=None):

        if word_df is None:
            word_df = load_equivalence_class_matrix()
        self.word_df = word_df
        self.solution_words = get_solution_words(return_type='pd')
        self.solutions_df = self.word_df.loc[:, self.solution_words].transpose()
        self.history = OrderedDict()

    def reset(self):  # Returns game to initial state, wiping history
        self.solutions_df = self.word_df.loc[:, self.solution_words].transpose()
        self.history = OrderedDict()

    def guess(self, guess_word):  # Pass a guess, return EC pattern + update state
        guess_word_series = self.solutions_df[guess_word]
        bins = guess_word_series.value_counts()
        potential_responses = bins[bins == bins.max()].index # Max values
        if len(potential_responses) > 0:
            response_pattern = self.break_ties(potential_responses)
        else:
            response_pattern = bins.max()  # Response pattern maximum solution words
        self.history[guess_word] = self.solutions_df
        self.solutions_df = self.solutions_df[self.solutions_df[guess_word] == response_pattern]
        return response_pattern

    def guesses(self, guesses):
        for guess in guesses:
            r = self.guess(guess)
            if len(self.solutions_df) == 1:
                return 1
        return len(self.solutions_df)


    def revert(self):
        #  removes the last guess for backtracking
        if len(self.history.keys()) > 0:
            i, self.solutions_df = self.history.popitem()
        else:
            self.reset()

    @staticmethod
    def break_ties(values):
        # Takes a list of values and applies tiebreaker rules
        #: // twitter.com / qntm / status / 1481071932154032134
        def min_let(vals, let):
            count = 5
            patterns = []
            for val in vals:
                c = val.count(let)
                if c == count:
                    patterns.append(val)
                if c < count:
                    count = c
                    patterns = [val]
            assert(len(patterns) > 0)
            return patterns
        values = list(values)
        for letter in ('G', 'Y'):
            values = min_let(values, letter)
            if len(values) == 1:
                return values[0]

        return values[0]
