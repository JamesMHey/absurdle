import unittest

import pandas as pd
from helpers import load_equivalence_class_matrix, get_valid_words, get_solution_words
from simulator import AbsurdleSimulator


class TestHelperMethods(unittest.TestCase):

    def test_get_valid_words(self):
        self.assertIn(list('FALSE'), get_valid_words('np'))
        self.assertIn('FALSE', get_valid_words('pd').values)  # Known problematic word
        self.assertIn('AAHED', get_valid_words('pd').values)  # Valid but non solution word

    def test_get_solution_words(self):
        self.assertIn(list('FALSE'), get_solution_words('np'))
        self.assertIn('FALSE', get_solution_words('pd').values)  # Known problematic word
        self.assertNotIn('AAHED', get_solution_words('pd').values)  # Valid but non solution word should not be in

    def test_load_equivalence_class_matrix(self):
        mx = load_equivalence_class_matrix()
        self.assertIsInstance(mx, pd.DataFrame)  # Ensure it is a dataframe
        self.assertGreater(len(mx), 0)  # Assert EC matrix has length
        self.assertIn("FALSE", mx.index)  # Known problematic string
        self.assertIn("FALSE", mx.columns)


class TestSimulatorMethods(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.sim = AbsurdleSimulator()

    @staticmethod
    def compare_index(df1, df2):  # Checks in two DF indexes are the same
        if len(df1.index) != len(df2.index):
            return False
        return (df1.index == df2.index).all()  # If lengths are the same, are all values the same?

    def test_revert(self):
        from copy import deepcopy
        guess_word = 'STYLE'
        orinial = deepcopy(self.sim)
        self.sim.guess(guess_word)
        self.assertEqual(self.compare_index(self.sim.solutions_df, orinial.solutions_df), False)  # Guess worked
        self.sim.revert()
        self.assertEqual(self.compare_index(self.sim.solutions_df, orinial.solutions_df), True)  # Reversion worked
        self.assertEqual(len(self.sim.history), len(orinial.history))
        self.sim.reset()

    def test_known_solution(self):
        solution = ['STYLE', 'BACON', 'AWARD', 'PIZZA']  # Known good solution (on orinial absurdle)
        answer = solution.pop()
        bin_size = self.sim.guesses(solution)
        self.assertEqual(bin_size, 1)  # simulator returns bin size of 1 before final guess
        self.assertEqual(len(self.sim.solutions_df), 1)  # Only one solution in solutions set
        self.assertIn(answer, self.sim.solutions_df.index.values)  # Final answer is in bin
        self.assertEqual(self.sim.guess(answer), 'GGGGG')  # Ensure correct response code
        self.sim.reset()

